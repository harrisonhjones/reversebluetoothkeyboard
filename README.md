# Reverse Bluetooth Keyboard
Harrison H. Jones
Harrison@hhj.me

## Purpose

The reverse Bluetooth keyboard project was born out of desperation. I was setting up a Raspberry Pi and I did not have a USB keyboard which was required to do the initial setup. All I needed to do was press <Enter> a couple of times and I could toss the keyboard away and SSH into the box but until I had the keyboard with which to press <Enter> I was stuck. I had been playing around with the idea of giving my android phone the ability to type passwords out when I was at a public computer so I had the required hardware to make this, albeit simplified, project a reality: a teensy, which can emulate a HID keyboard, and a Bluetooth serial module. A few lines of code later, and some squashed bugs, I was using my laptop's keyboard to press the required <Enter> keys on the Pi!

## Required Components

* Teensy (any version will do. You just need to be able to put it into Keyboard emulation mode)
* Bluetooth Module (I used a HC-05 module I picked up on eBay for about $5/ea)

## Wiring the components

The wiring is very very simple. The HC-05 module I have has 6 pins: Key, 5V, GND, RX, TX, and State. The 5V line goes to 5V on the Teensy, the GND goes to GND, the RX goes to the Teensy's TX (D3 on the Teensy 2.0), and the TX goes to the Teensy's RX (D2 on the Teensy 2.0). The other two pins are currently unused.

![visual wiring diagram](https://bitbucket.org/harrisonhjones/reversebluetoothkeyboard/raw/master/electronics/visualSchematic.png)

## Building the Code
I developed the code in the Arduino IDE using the Teensyduino interface PJRC has provided. Get it here: https://www.pjrc.com/teensy/teensyduino.html

* Open simpleKeyboard.ino in the Arduino IDE
* Plug in your Teensy
* Select a "USB Type" which contains the words "Keyboard" in the Tools >> USB Type menu
* Compile and upload the code to your Teensy


## Using the Final Product

* Plug in the Reverse Bluetooth Keyboard module into the computer you wish to use it on. 
* Pair with the Bluetooth Module on another computer or phone
* Open up a terminal program on the computer or phone and begin typing. All ASCII characters should come across loud and clear. 


## Known Issues

* Because I also needed to use the arrow keys, which are not standard ASCII characters, I replaced some standard keyboard characters with the arrow keys. "[" is up, ";" is left, "'" is right, "/" is down and <Enter> puts a CR and a LF. This can easily be changed in code. If something thinks of a better way feel free to shoot me a pull request!