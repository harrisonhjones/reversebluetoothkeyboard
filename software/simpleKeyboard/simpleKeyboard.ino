/* Simple Keyboard
Pipes out incoming characters from the Bluetooth serial connection over an emulated HID keyboard

Changelog
1.0 - 7/15/2014 - Initial Release

*/

// Use the Teensy's hardware serial port
#define HWSERIAL Serial1

void setup() {
  // Setup the hardware serial to use the Bluetooth serial module's default baud rate of 9600
  HWSERIAL.begin(9600);
}

void loop() {
  // Declare a temp variable to hold an incoming character from the Bluetooth adapter
  int incomingByte = 0;

  // If there are bytes waiting on the hardware serial line read them
	if (HWSERIAL.available() > 0) {
    // Read the first available incoming byte
		incomingByte = HWSERIAL.read();

    // Let the operator know the byte was received. 
    HWSERIAL.print("OK[");
    HWSERIAL.print(incomingByte, DEC);
    HWSERIAL.println("]");
    
    // If the byte is a "[" character replace it with a UP arrow press
    if(incomingByte == 91)
    {
      Keyboard.set_key1(KEY_UP);
      Keyboard.send_now();
      Keyboard.set_key1(0);
      Keyboard.send_now();
    }
    // If the byte is a ";" character replace it with a LEFT arrow press
    else if(incomingByte == 59)
    {
      Keyboard.set_key1(KEY_LEFT);
      Keyboard.send_now();
      Keyboard.set_key1(0);
      Keyboard.send_now();
    }
    // If the byte is a "'['" character replace it with a RIGHT arrow press
    else if(incomingByte == 39)
    {
      Keyboard.set_key1(KEY_RIGHT);
      Keyboard.send_now();
      Keyboard.set_key1(0);
      Keyboard.send_now();
    }
    // If the byte is a "/" character replace it with a DOWN arrow press
    else if(incomingByte == 47)
    {
      Keyboard.set_key1(KEY_DOWN);
      Keyboard.send_now();
      Keyboard.set_key1(0);
      Keyboard.send_now();
    }
    // If the byte is a CR character replace it with a CR and LF
    else if(incomingByte == 13)
    {
      Keyboard.write(13);
      Keyboard.write(10);
    }
    // If it's just a plain old ascii character we do not wish to modile write it over the keyboard.
    else
    {
      Keyboard.write(incomingByte);    
    }
  }
}

